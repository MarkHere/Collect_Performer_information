# Collect_Performer_information
Collect area, contact info, activity and equipment of performer in the city. [Multiple account, IP blocked, Header parameter]

## User :
Run the program  
![image](https://gitlab.com/MarkHere/Collect_Performer_information/-/raw/master/Collect_Performers_info.png)
![image](https://gitlab.com/MarkHere/Collect_Performer_information/-/raw/master/IG_Host_Tag.gif)

## Backstage : Python + Selenium + Request + Beautifulsoup + UserAgent
For example: Hoster  
Phase 1 : Use chromedriver, search #hoster, scroll down for dynamically loading, record all post link.  
Phase 2 : Send "get" to the link and then get the author's ID of each post.  
Phase 3 : Use mulitple threading to login each account then search the ID main page to get the user information.  
IP blocked : sometime if you send each request too quick and too close, IG will block your IP, the solution I used is to make a randomly stop between each request. 
